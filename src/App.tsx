import './App.css'
import React, { useEffect } from 'react'
import { fabric } from 'fabric'
import { useAppDispatch, useAppSelector } from './app/hooks'
import { v4 as uuidv4 } from 'uuid'
import { BodyType, RectangleType, CircleType, ShapeType } from './features/drawing/drawingTypes'
import { add, move } from './features/drawing/drawingSlice'

const HEIGHT = 600
const WIDTH = 1200
const GRID = 20

const px2grid = (px: number): number => Math.round(px / GRID)
const grid2px = (x: number): number => x * GRID

export default function App() {
  const drawing = useAppSelector((state) => state.drawing)

  const dispatch = useAppDispatch()

  const putBody = (shape: ShapeType): fabric.Rect | fabric.Circle => {
    const id = uuidv4()
    dispatch(add({ ...shape, id }))

    const { x, y, color, type } = shape

    const info = {
      left: grid2px(x),
      top: grid2px(y),
      fill: color || 'black',
      originX: 'center',
      originY: 'center',
      centeredRotation: true,
      hasControls: false,
      id,
    }

    if (type === BodyType.Rect) {
      shape = shape as RectangleType
      return new fabric.Rect({
        ...info,
        width: grid2px(shape.width),
        height: grid2px(shape.height),
      })
    }
    shape = shape as CircleType
    return new fabric.Circle({
      ...info,
      radius: grid2px(shape.radius),
    })
  }

  React.useEffect(() => {
    const canvas = new fabric.Canvas('my-fabric-canvas', {
      height: HEIGHT,
      width: WIDTH,
      selection: false,
    })

    const grid = 20
    const step = 5

    // create grid

    for (let i = 0; i < WIDTH / grid; i++) {
      i % step
        ? canvas.add(
            new fabric.Line([i * grid, 0, i * grid, HEIGHT], { stroke: '#ddd', selectable: false })
          )
        : canvas.add(
            new fabric.Line([i * grid, 0, i * grid, HEIGHT], { stroke: '#777', selectable: false })
          )
    }

    for (let i = 0; i < HEIGHT / grid; i++) {
      i % step
        ? canvas.add(
            new fabric.Line([0, i * grid, WIDTH, i * grid], { stroke: '#ddd', selectable: false })
          )
        : canvas.add(
            new fabric.Line([0, i * grid, WIDTH, i * grid], { stroke: '#777', selectable: false })
          )
    }

    // add objects

    canvas.add(putBody({ x: 1, y: 1, width: 4, height: 2, color: '#ddd', type: BodyType.Rect }))
    canvas.add(putBody({ x: 10, y: 10, width: 4, height: 2, color: '#ddd', type: BodyType.Rect }))
    canvas.add(putBody({ x: 15, y: 15, width: 4, height: 2, color: '#ddd', type: BodyType.Rect }))
    canvas.add(putBody({ x: 15, y: 15, radius: 0.5, type: BodyType.Circle }))

    // snap to grid

    canvas.on('object:moving', function (options: fabric.IEvent) {
      options?.target?.set({
        left: Math.max(Math.min(px2grid(Number(options?.target?.left ?? 0)) * GRID, WIDTH), 0),
        top: Math.max(Math.min(px2grid(Number(options?.target?.top ?? 0)) * GRID, HEIGHT), 0),
      })
    })

    canvas.on('object:moved', function (options: fabric.IEvent) {
      // messed up library
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const temp = options.target as any
      dispatch(
        move({
          x: px2grid(options?.target?.left || 0),
          y: px2grid(options?.target?.top || 0),
          id: temp?.id,
        })
      )
    })

    // UseEffect's cleanup function
    return () => {
      canvas.dispose()
    }
  }, [])

  useEffect(() => console.log(drawing), [drawing])

  return (
    <div className="App">
      <canvas id="my-fabric-canvas" width={WIDTH + 100} height={HEIGHT + 100} />
    </div>
  )
}
