import { configureStore } from '@reduxjs/toolkit'
import drawingSlice from '../features/drawing/drawingSlice'

const store = configureStore({ reducer: { drawing: drawingSlice } })

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export default store
