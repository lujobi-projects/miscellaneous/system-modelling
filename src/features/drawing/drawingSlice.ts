import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ShapeMovementType, ShapeType } from './drawingTypes'

type DrawingState = ShapeType[]

// Define the initial state using that type
const initialState: DrawingState = []

export const drawingSlice = createSlice({
  name: 'drawing',
  initialState,
  reducers: {
    add: (state: DrawingState, action: PayloadAction<ShapeType>) => {
      state.push(action.payload)
    },
    move: (state: DrawingState, action: PayloadAction<ShapeMovementType>) => {
      if (!action.payload) return
      const idx = state.findIndex((s) => s.id == action.payload.id)
      state[idx].x = action.payload.x
      state[idx].y = action.payload.y
    },
    reset: (state: DrawingState) => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      state = initialState
    },
  },
})
// Action creators are generated for each case reducer function
export const { add, move } = drawingSlice.actions
export default drawingSlice.reducer
