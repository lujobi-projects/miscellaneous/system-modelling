export enum BodyType {
  Rect = 'Rect',
  Circle = 'Circle',
  Arrow = 'Arrow',
}

export interface ShapePositionType {
  x: number
  y: number
}
export interface ShapeMovementType extends ShapePositionType {
  id: string
}

export interface BaseShapeType extends ShapePositionType {
  color?: string
  type: BodyType
  id?: string
}

export interface RectangleType extends BaseShapeType {
  width: number
  height: number
}

export interface ArrowType extends BaseShapeType {
  end_x: number
  end_y: number
}

export interface CircleType extends BaseShapeType {
  radius: number
}

export type ShapeType = RectangleType | CircleType | ArrowType
